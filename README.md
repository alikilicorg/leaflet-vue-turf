# Leaflet Education #

This repository was produced as a code base for leaflet training.

### Which software technologies were used? ###

* [HTML5](https://www.w3schools.com/html/html5_intro.asp), [CSS](https://www.w3schools.com/css/default.asp), [Javascript](https://www.w3schools.com/js/default.asp) 
* [Bootstrap CSS Library](https://getbootstrap.com/docs/3.3/) 
* [Leaflet Map Library](https://leafletjs.com/)
* [Turf js Geospatial Analysis Library](http://turfjs.org/)
* [Vue js Javascript Framework](https://vuejs.org/)
* [NodeJS](https://nodejs.org/en/)

### Which IDE were used? ###

* [Visual Studio Code](https://code.visualstudio.com/) 
* [Notepad ++](https://notepad-plus-plus.org/download/v7.5.7.html) 

### Training Planning ###

* Leflet'i Neden ve Nerede Kullanmalıyız
* Laflet Nasıl Yüklenir
* 

### Important Links ###

* Udemy Education Link
* Author Link